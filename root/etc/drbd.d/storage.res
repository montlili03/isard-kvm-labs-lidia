resource storage
{
    #template-file "linstor_common.conf";

    handlers
    {
        fence-peer "/usr/lib/drbd/crm-fence-peer.9.sh";
        unfence-peer "/usr/lib/drbd/crm-unfence-peer.9.sh";
    }

    options
    {
        quorum off;
    }
    
    net
    {
        cram-hmac-alg     sha1;
        shared-secret     "4XtZfDZHuU796UwYb31y";
        allow-two-primaries yes;
        fencing resource-and-stonith;

    }
    
    on cluster1
    {
        volume 0
        {
            disk        /dev/md0;
            disk
            {
                discard-zeroes-if-aligned yes;
                rs-discard-granularity 8192;
            }
            meta-disk   internal;
            device      minor 1000;
        }
        node-id    1;
        address 172.31.1.101:7000;
    }
    
    on cluster2
    {
        volume 0
        {
            disk        /dev/md0;
            disk
            {
                discard-zeroes-if-aligned yes;
                rs-discard-granularity 8192;
            }
            meta-disk   internal;
            device      minor 1000;
        }
        node-id    2;
        address 172.31.1.102:7000;
    }

    on cluster3
    {
        volume 0
        {
            disk        /dev/md0;
            disk
            {
                discard-zeroes-if-aligned yes;
                rs-discard-granularity 8192;
            }
            meta-disk   internal;
            device      minor 1000;
        }
        node-id    3;
        address 172.31.1.103:7000;
    }


    connection-mesh
    {
        hosts cluster1 cluster2 cluster3;
    }
}
